<?php
// $Id: stumble.admin.inc,v 1.1.2.3 2008/10/23 05:19:37 davereid Exp $

/**
 * @file
 * Admin page callbacks for the stumble module.
 */

/**
 * Admin settings form.
 *
 * @see system_settings_form()
 */
function stumbled_settings_form() {
  $form['stumbled_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable stumbling for the following content types'),
    '#options' => _node_types_build()->names,
    '#default_value' => stumbled_variable_get('stumbled_node_types'),
  );
  $form['stumbled_same_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only stumble between the same content types.'),
    '#default_value' => stumbled_variable_get('stumbled_same_type'),
  );
  $form['stumbled_prefer_unread'] = array(
    '#type' => 'checkbox',
    '#title' => t('Stumble to unread nodes first.'),
    '#description' => t('If no unread nodes are found, a random node will be selected.'),
    '#default_value' => stumbled_variable_get('stumbled_prefer_unread'),
  );
  $form['stumbled_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Text'),
    '#default_value' => stumbled_variable_get('stumbled_link_text'),
    '#size' => 20,
  );
  $form['stumbled_block_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Text for Block'),
    '#default_value' => stumbled_variable_get('stumbled_block_link_text'),
    '#size' => 20,
  );
  $form['stumbled_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#default_value' => stumbled_variable_get('stumbled_message'),
    '#description' => t('A message to show after a user is stumbled. Leave blank for no messages.'),
    '#size' => 100,
  );
  $form['array_filter'] = array('#type' => 'value', '#value' => TRUE);

  return system_settings_form($form);
}
