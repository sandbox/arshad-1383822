<?php
// $Id$

/**
 * @file
 * Enables users to 'stumble' to random content.
 */

/**
 * Implements hook_menu().
 */
function stumbled_menu() {
  $items['stumble'] = array(
    'title' => 'Stumble',
    'description' => 'Go to a random page.',
    'page callback' => 'stumbled_go',
    'access arguments' => array('access content'),
    'type' => MENU_SUGGESTED_ITEM,
  );
  $items['admin/config/stumble'] = array(
    'title' => 'Stumble',
    'description' => 'Settings for stumbling to random content.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stumbled_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'stumbled.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_node_view().
 */
function stumbled_node_view($node, $view_mode, $langcode) {
  if ($view_mode != 'teaser' && in_array($node->type, stumbled_variable_get('stumbled_node_types'))) {
    $node->content['links']['node']['#links']['stumble'] = array(
      'title' => stumbled_variable_get('stumbled_link_text'),
      'href' => 'stumble',
      'query' => array('nid' => $node->nid, 'type' => $node->type),
      'attributes' => array(
        'title' => t('View a random page on this site.'),
        'class' => 'stumble',
      ),
    );
  }
}

/**
 * Implements hook_block_info().
 */
function stumbled_block_info() {
  return array(
    'stumbled_link' => array(
      'info' => t('Stumble Link'),
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function stumbled_block_view($delta = '') {
  $block = array();
  
  switch ($delta) {
    case 'stumbled_link':
      $block['subject'] = '';
      $block['content'] = l(stumbled_variable_get('stumbled_block_link_text'), 'stumble', array(
        'attributes' => array(
          'id' => 'stumble-link',
        ),
      ));
      break;
  }
  
  return $block;
}

/**
 * Finds a random node based on current node type and options, and redirects
 * to that node.
 */
function stumbled_go() {
  global $user;
  $node_type = isset($_GET['type']) ? $_GET['type'] : '';
  $node_nid = isset($_GET['nid']) && is_numeric($_GET['nid']) ? (int) $_GET['nid'] : 0;
  $node_types = array($node_type);

  if (empty($node_type) || !stumbled_variable_get('stumbled_same_type')) {
    $node_types = stumbled_variable_get('stumbled_node_types');
  }
  
  //Prefer unread nodes first if selected and user is logged in
  if ($user->uid && stumbled_variable_get('stumbled_prefer_unread')) {
    $sql = db_select('node', 'n');
    $sql->addExpression('COUNT(n.nid)', 'count');
    $sql->addExpression('MAX(n.nid)', 'max');
    $sql->addExpression('MIN(n.nid)', 'min');
    $sql->condition('n.nid', $node_nid, '<>');
    
    //find read nodes
    $unread_nodes = db_select('history', 'h')
      ->fields('h', array('nid'))
      ->condition('uid', $user->uid)
      ->execute()
      ->fetchCol('nid');
    
    $sql->condition('n.nid', $unread_nodes, 'NOT IN');
    $sql->condition('n.status', 1);
    $sql->condition('n.type', $node_types);
    $nodes = $sql->execute()
      ->fetchAssoc();
  }
  
  //if no unread node found, find a range from all nodes
  if ($nodes['count'] == 0) {
    // Find target range
    $sql = db_select('node', 'n');
    $sql->addExpression('COUNT(n.nid)', 'count');
    $sql->addExpression('MAX(n.nid)', 'max');
    $sql->addExpression('MIN(n.nid)', 'min');
    $sql->condition('n.nid', $node_nid, '<>');
    $sql->condition('n.status', 1);
    $sql->condition('n.type', $node_types);
    $nodes = $sql->execute()
      ->fetchAssoc();
  }
    
  if ($nodes && $nodes['count'] > 0) {
    $sql = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('nid', array($nodes['min'], $nodes['max']), 'between')
      ->condition('nid', $node_nid, '<>')
      ->condition('n.type', $node_types)
      ->range(mt_rand(0, $nodes['count'] - 1), 1);
      
    $stumbled_nid = $sql->execute()
      ->fetchField();
    
    //set a stumble message
    if (stumbled_variable_get('stumbled_message') != '') {
      drupal_set_message(stumbled_variable_get('stumbled_message'));
    }
    
    //redirect to node
    drupal_goto('node/'. $stumbled_nid);
  }
  
  //Failed, go back to frontpage
  drupal_goto($node_nid ? 'node/'. $node_nid : '<front>');
}

/**
 * Internal default variables for stumbled_variable_get().
 */
function stumbled_variables() {
  return array(
    'stumbled_node_types' => array_keys(_node_types_build()->names),
    'stumbled_same_type' => 0,
    'stumbled_prefer_unread' => 1,
    'stumbled_link_text' => t('Stumble'),
    'stumbled_block_link_text' => t('Stumble'),
    'stumbled_message' => '',
  );
}

/**
 * Internal implementation of variable_get().
 */
function stumbled_variable_get($name) {
  static $defaults = NULL;
  if (!isset($defaults)) {
    $defaults = stumbled_variables();
  }
  if (!isset($defaults[$name])) {
    watchdog('stumble', 'Default variable for %variable not found.', array('%variable' => $name));
    return NULL;
  }
  else {
    return variable_get($name, $defaults[$name]);
  }
}